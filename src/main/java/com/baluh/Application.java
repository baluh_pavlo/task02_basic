package com.baluh;

import java.util.Scanner;

/**
 * <h1>Numbers</h1>
 * This program has the following functionality:
 * <ul>
 * <li>Program prints odd numbers from start to the end of interval and even from end to start
 * (you need to enter the interval first)</li>
 * <li>Program prints the sum of odd and even numbers</li>
 * <li>Program build Fibonacci numbers which starts from the biggest odd and the biggest even numbers
 * from your interval in size that you enter</li>
 * <li>Program prints percentage off odd and even numbers from fibonacci sequence</li>
 * </ul>
 *
 * @author Baluh Pavlo
 * @version 1.0
 * @since 08.11.2019
 */

public class Application {
    /**
     * This is main method it use NumberService Class to process the data and display it
     *
     * @param args main method parameters
     * @throws NumberFormatException          On Input exceptions
     * @throws ArrayIndexOutOfBoundsException On Input exceptions
     * @throws FromBiggerThanToException      On Input exceptions
     */

    public static void main(String[] args) {
        Integer from;
        Integer to;
        System.out.println("Enter the interval: ");
        Scanner scanner = new Scanner(System.in);
        String interval = scanner.nextLine();
        try {
            from = Integer.parseInt(interval.split(" ")[0]);
            to = Integer.parseInt(interval.split(" ")[1]);
            if (from > to) {
                throw new FromBiggerThanToException();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Enter two numbers!");
            return;
        } catch (NumberFormatException e) {
            System.out.println("Only numbers!");
            return;
        } catch (FromBiggerThanToException e) {
            System.out.println("From cant be bigger than To");
            return;
        }
        NumbersService numbersService = new NumbersService(from, to);
        System.out.println("Odd numbers from start: ");
        System.out.println(numbersService.getOddNumbersFromStart());
        System.out.println("Even numbers from end: ");
        System.out.println(numbersService.getEvenNumbersFromEnd());
        System.out.println("Sum of odd numbers");
        System.out.println(numbersService.getSumOfOddNumbers());
        System.out.println("Sum of even numbers");
        System.out.println(numbersService.getSumOfEvenNumbers());
        System.out.println("Enter size of set: ");
        int i = scanner.nextInt();
        System.out.println("Fibonacci numbers: ");
        System.out.println(numbersService.buildFibonacciNumbers(i));
        System.out.println("Percentage of odd numbers ");
        System.out.println(numbersService.getPercentageOfOddFibonacci() + "%");
        System.out.println("Percentage of even numbers ");
        System.out.println(numbersService.getPercentageOfEvenFibonacci() + "%");
    }
}
