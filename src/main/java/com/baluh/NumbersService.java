package com.baluh;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>Service</h1>
 * <p>This class using for calculate data for Numbers Program</p>
 *
 * @author Baluh Pavlo
 * @version 1.0
 * @since 08.11.2019
 */

public class NumbersService {
    /**
     * this variable contains array of numbers from specified range
     */
    private List<Integer> numbersArray = new ArrayList<Integer>();

    /**
     * this variable contains array of fibonacci numbers from specified range
     */
    private List<Integer> fibonacciArray = new ArrayList<Integer>();

    /**
     * constructor Initializes numbers array
     *
     * @param from This is the first element of range
     * @param to   This is the first element of range
     */

    public NumbersService(Integer from, Integer to) {
        for (int i = from; i <= to; i++) {
            numbersArray.add(i);
        }
    }

    /**
     * This method such only odd numbers from numbersArray
     *
     * @return List of Integer method return odd numbers
     */

    public List<Integer> getOddNumbersFromStart() {
        return numbersArray.stream().filter((e) -> e % 2 != 0).collect(Collectors.toList());
    }

    /**
     * This method such only even numbers from numbersArray
     *
     * @return List of Integer  method return even numbers
     */

    public List<Integer> getEvenNumbersFromEnd() {
        return numbersArray.stream().filter((e) -> e % 2 == 0).
                sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }

    /**
     * This method calculate sum of only odd numbers from numbersArray
     *
     * @return Integer method return sum of odd numbers;
     */

    public Integer getSumOfOddNumbers() {
        Integer sum = 0;
        List<Integer> collected = getOddNumbersFromStart();
        for (Integer integer : collected) {
            sum += integer;
        }
        return sum;
    }

    /**
     * This method calculate sum of only even numbers from numbersArray
     *
     * @return Integer method return sum of even numbers;
     */

    public Integer getSumOfEvenNumbers() {
        Integer sum = 0;
        List<Integer> collected = getEvenNumbersFromEnd();
        for (Integer integer : collected) {
            sum += integer;
        }
        return sum;
    }

    /**
     * This method build fibonacci sequence with specified length which starts from
     * the biggest odd and the biggest even numbers from your interval
     *
     * @param length This is length of fibonacci sequence
     * @return List of Integer method return List of  fibonacci numbers
     */

    public List<Integer> buildFibonacciNumbers(Integer length) {
        Integer[] fibonacciArray = new Integer[length];
        fibonacciArray[0] = Collections.max(getEvenNumbersFromEnd());
        fibonacciArray[1] = Collections.max(getOddNumbersFromStart());
        for (int i = 2; i < length; i++) {
            fibonacciArray[i] = fibonacciArray[i - 2] + fibonacciArray[i - 1];
        }
        this.fibonacciArray = Arrays.asList(fibonacciArray);
        return this.fibonacciArray;
    }

    /**
     * This method calculate percentage of even fibonacci numbers
     *
     * @return Float method return percentage of even fibonacci numbers
     */

    public Float getPercentageOfEvenFibonacci() {
        float evenCount = 0;
        for (Integer integer : fibonacciArray) {
            if (integer % 2 == 0) {
                evenCount++;
            }
        }
        return (100 * evenCount) / fibonacciArray.size();
    }

    /**
     * This method calculate percentage of odd fibonacci numbers
     *
     * @return Float method return percentage of odd fibonacci numbers
     */

    public Float getPercentageOfOddFibonacci() {
        float oddCount = 0;
        for (Integer integer : fibonacciArray) {
            if (integer % 2 != 0) {
                oddCount++;
            }
        }
        return (100 * oddCount) / fibonacciArray.size();
    }


}
